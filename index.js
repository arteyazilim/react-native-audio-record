import { NativeModules, NativeEventEmitter } from 'react-native';
const { TTAudioRecord } = NativeModules;
const EventEmitter = new NativeEventEmitter(TTAudioRecord);

const AudioRecord = {};

AudioRecord.init = () => TTAudioRecord.init();
AudioRecord.start = () => TTAudioRecord.start();
AudioRecord.stop = () => TTAudioRecord.stop();
AudioRecord.on = (event, callback) => {
    return EventEmitter.addListener(event, callback);
};
AudioRecord.off = (event) => {
    return EventEmitter.removeAllListeners(event);
};

export default AudioRecord;
