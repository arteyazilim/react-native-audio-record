declare module "react-native-audio-record" {
  export interface IAudioRecord {
    init: (options: Options) => void
    start: () => void
    stop: () => void
    on: (event: string, callback: (data: object) => void) => void
    off: (event: string) => void
  }

  export interface Options {
    sampleRate: number
    /**
     * - `1 | 2`
     */
    channels: number
    /**
     * - `8 | 16`
     */
    bitsPerSample: number
    /**
     * - `6`
     */
    audioSource?: number
  }

  const AudioRecord: IAudioRecord

  export default AudioRecord;
}
