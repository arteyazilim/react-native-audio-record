package com.trtalk.audiorecord;

/**
 * This class is largely based on the IOCipher example (https://github.com/n8fr8/IOCipherCameraExample).
 * @author Nathan of the Guardian Project
 */

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaRecorder.AudioSource;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class TTAudioRecordModule extends ReactContextBaseJavaModule
{
    private static final String TAG = "TTAudioRecord";
    private final ReactApplicationContext reactContext;
    private DeviceEventManagerModule.RCTDeviceEventEmitter eventEmitter;

    private static final int BIT_RATE = 32000;
    private static final int SAMPLE_RATE = 44100;

    private AudioRecord recorder;
    private MediaCodec encoder;

    private int bufferSize;
    private boolean isRecording;

    public TTAudioRecordModule(ReactApplicationContext reactContext)
    {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "TTAudioRecord";
    }

    @ReactMethod
    public boolean init() throws Exception
    {
        eventEmitter = reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class);
        eventEmitter.emit("init", "");

        if (!initAudioRecord()) {
            return false;
        }
        encoder = MediaCodec.createEncoderByType("audio/mp4a-latm");
        MediaFormat format = new MediaFormat();
        format.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
        format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
        format.setInteger(MediaFormat.KEY_SAMPLE_RATE, TTAudioRecordModule.SAMPLE_RATE);
        format.setInteger(MediaFormat.KEY_BIT_RATE, TTAudioRecordModule.BIT_RATE);
        format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectHE);
        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        return true;
    }

    private boolean initAudioRecord()
    {
        try {
            bufferSize = AudioRecord.getMinBufferSize(TTAudioRecordModule.SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
            if (bufferSize != AudioRecord.ERROR_BAD_VALUE) {
                recorder = new AudioRecord(AudioSource.VOICE_RECOGNITION, TTAudioRecordModule.SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);
                if (recorder.getState() == AudioRecord.STATE_INITIALIZED) {
                    return true;
                }
            }
        } catch (Exception e) {
            eventEmitter.emit("error", "AudioRecord construction error.");
        }
        return false;
    }

    @ReactMethod
    public void start() throws IOException
    {
        int pcmlength;

        byte[] buffer = new byte[bufferSize];

        ByteBuffer[] inputBuffers;
        ByteBuffer[] outputBuffers;

        ByteBuffer inputBuffer;
        ByteBuffer outputBuffer;

        MediaCodec.BufferInfo bufferInfo;
        int inputBufferIndex;
        int outputBufferIndex;

        byte[] data;

        encoder.start();
        recorder.startRecording();
        isRecording = true;

        while (isRecording) {
            pcmlength = recorder.read(buffer, 0, bufferSize);
            eventEmitter.emit("pcmlength", pcmlength);

            inputBuffers = encoder.getInputBuffers();
            outputBuffers = encoder.getOutputBuffers();
            inputBufferIndex = encoder.dequeueInputBuffer(-1);

            if (inputBufferIndex >= 0) {
                inputBuffer = inputBuffers[inputBufferIndex];
                inputBuffer.clear();
                inputBuffer.put(buffer);
                encoder.queueInputBuffer(inputBufferIndex, 0, buffer.length, 0, 0);
            }

            bufferInfo = new MediaCodec.BufferInfo();
            outputBufferIndex = encoder.dequeueOutputBuffer(bufferInfo, 0);

            while (outputBufferIndex >= 0) {
                outputBuffer = outputBuffers[outputBufferIndex];
                outputBuffer.position(bufferInfo.offset);
                outputBuffer.limit(bufferInfo.offset + bufferInfo.size);
                data = new byte[bufferInfo.size];
                outputBuffer.get(data);
                eventEmitter.emit("data", Base64.encodeToString(data, Base64.NO_WRAP));
                encoder.releaseOutputBuffer(outputBufferIndex, false);
                outputBufferIndex = encoder.dequeueOutputBuffer(bufferInfo, 0);
            }
        }
        encoder.stop();
        recorder.stop();
        eventEmitter.emit("finish", "");
    }

    @ReactMethod
    public void stop()
    {
        isRecording = false;
    }

    protected void onDestroy()
    {
        recorder.release();
        encoder.release();
    }
}
