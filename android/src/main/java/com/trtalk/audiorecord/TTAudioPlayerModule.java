package com.trtalk.audiorecord;

/**
 * This class is largely based on the IOCipher example (https://github.com/n8fr8/IOCipherCameraExample).
 * @author Nathan of the Guardian Project
 */

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaRecorder.AudioSource;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

public class TTAudioPlayerModule extends ReactContextBaseJavaModule
{
    private static final String TAG = "TTAudioPlayer";
    private final ReactApplicationContext reactContext;
    private DeviceEventManagerModule.RCTDeviceEventEmitter eventEmitter;

    private static final int BIT_RATE = 32000;
    private static final int SAMPLE_RATE = 44100;

    private AudioTrack player;
    private MediaCodec decoder;

    ByteBuffer[] inputBuffers;
    ByteBuffer[] outputBuffers;

    ByteBuffer inputBuffer;
    ByteBuffer outputBuffer;

    MediaCodec.BufferInfo bufferInfo;
    int inputBufferIndex;
    int outputBufferIndex;
    byte[] outData;
    boolean isStarted;

    public TTAudioPlayerModule(ReactApplicationContext reactContext)
    {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "TTAudioPlayer";
    }

    @ReactMethod
    public boolean init() throws Exception
    {
        isStarted = false;
        eventEmitter = reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class);
        eventEmitter.emit("init", "");

        if (!initAudioPlayer()) {
            eventEmitter.emit("error", "AudioTrack couldnot load.");
            return false;
        }

        MediaFormat format = new MediaFormat();
        format.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
        format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
        format.setInteger(MediaFormat.KEY_SAMPLE_RATE, TTAudioPlayerModule.SAMPLE_RATE);
        format.setInteger(MediaFormat.KEY_BIT_RATE, TTAudioPlayerModule.BIT_RATE);
        format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectHE);

        try {
            decoder = MediaCodec.createDecoderByType("audio/mp4a-latm");
            decoder.configure(format, null, null, 0);
        } catch (IOException e) {
            eventEmitter.emit("error", e.toString());
            return false;
        }

        return true;
    }

    private boolean initAudioPlayer()
    {
        int bufferSizePlayer = AudioTrack.getMinBufferSize(TTAudioPlayerModule.SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        player = new AudioTrack(AudioManager.STREAM_MUSIC, TTAudioPlayerModule.SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSizePlayer, AudioTrack.MODE_STREAM);
        if (player.getState() == AudioTrack.STATE_INITIALIZED) {
            return true;
        }
        return false;
    }

    public void start()
    {
        isStarted = true;
        player.play();
        decoder.start();
    }

    public void write(byte[] data)
    {
        inputBuffers = decoder.getInputBuffers();
        outputBuffers = decoder.getOutputBuffers();
        inputBufferIndex = decoder.dequeueInputBuffer(-1);

        if (inputBufferIndex >= 0) {
            inputBuffer = inputBuffers[inputBufferIndex];
            inputBuffer.clear();
            inputBuffer.put(data);
            decoder.queueInputBuffer(inputBufferIndex, 0, data.length, 0, 0);
        }

        bufferInfo = new MediaCodec.BufferInfo();
        outputBufferIndex = decoder.dequeueOutputBuffer(bufferInfo, 0);

        while (outputBufferIndex >= 0) {
            outputBuffer = outputBuffers[outputBufferIndex];

            outputBuffer.position(bufferInfo.offset);
            outputBuffer.limit(bufferInfo.offset + bufferInfo.size);

            outData = new byte[bufferInfo.size];
            outputBuffer.get(outData);

            player.write(outData, 0, outData.length);

            decoder.releaseOutputBuffer(outputBufferIndex, false);
            outputBufferIndex = decoder.dequeueOutputBuffer(bufferInfo, 0);
        }
    }

    public void stop()
    {
        decoder.stop();
        player.stop();
        isStarted = false;
    }

    protected void onDestroy()
    {
        player.release();
        decoder.release();
    }
}
